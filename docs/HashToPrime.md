**HOW TO PROOF THERE'S A HASH IN A LIST OF HASHES WITH zk-SNARK** by Manuel Pérez

The world of cryptography it's in continuous expansion. Always we can find new necessities. Today, we present a new way to proof that we own the information of a Claim in a list of Claims, using a new method: zk-SNARK.

**INTRODUCTION**

This pretend to be a method to demonstrate to external people that you have a Claim (or hash) in a list of those elements. At the practice, an example can be an Identity: how I can proof to you that I am an adult without showing my date of birth or my identity card? Proving that I have in my possession this document. The idea seems very distorted, but it's simple: so far, the only way to prove you have a document (in a tech structure), is giving the hash of this document. We propose a method that allows us to prove it without revealing anything.

**HOW DOES IT WORK?**


The way we propose is simple. What known tool allow to prove some fact without reveal confidential information? The zk-SNARK. This would be the nucleus of all we describe in this post. But how? With a numerical assignment to each hash.



The idea is the following: imagine that we want to prove that we have some information or document. To prove it, we show that we have it without the need to reveal ABSOLUTELY nothing. We will generate a number to each Claim and, through an operation, we will produce an other number depending of the others. The most efficient way is assigning prime numbers and multiply it. Why? Because then the decomposition of the number produced will be unique.



What we want to do is pas the hash of the Claim that we want to prove we have in our possession and it's in the list. If we think in sum the numbers, any number can subtract from the SNARK, and it does not necessarily mean that we know the numbers assigned to the elements (for example, if I pass to you the number 42, 5, and I say to you that there's 11 commands, it doesn't mean that you know the other 10 commands. There's a lot of possibilities). That's what the zk-SNARK will take care of: it take the prime assigned to the Claim and catches if this prime divides at the number of the SNARK. If it divides it, that's means that the Claim or document it's in the list; if not, we don't have it.

**SPECIFICATIONS**

Being a product, we need to fix a $n_{min}$ and a $n_{max}$, that it appoints the maximum and minimum quantity of Claims in our possession. It's a necessity, because we multiply numbers (in the best case, huge primes), and it increase quickly the power of 2. We know we can't overcome $2^{256}$. 

-$n_{max}$ will be determined by the maximum rank we want to the prime generated. For example, if we want that will be as much $2^{32}$, we can only have 8 Claims, so 256/8=32. 
-Obviously $n_{min}$ will be greater or equal to 2 so we can form a list. 

![](https://i.imgur.com/pOQ4eO4.jpg)


**THE ASSIGNMENT**

In this point we will explained how we assign a prime number to a hash. 

1) Firstly we convert the hash $H$ in a number. We can find lot of methods to do it. We opted by the conversion of hexadecimal to decimal basis.
2) Once we have this number $n$, we calculate the residue of the quotient between 32 (i.e.. e., calculate $n\equiv d \ (mod \ 32)$).
3) Calculate $x=2^d-1$ and $y=2^{d+1}-1$.
4) Pick $p\in [x,y]$, with $p$ prime. This would be our assignation: $H\rightarrow p$.

SOME EXAMPLES.

--------------------------------------------------------------------------
Enter string: 297308279EB559791E9E37F649CED81CB82EDE82B8E71F4F78553ECEB5483BCD

The hash value is 893926349, which is congruent with 13 modulus 32
The prime associated is 8191.

--------------------------------------------------------------------------

Enter string: 8440B63EE3474678C1DC78D58C704439D27A058D538D32F06E6914CA8DB9D0CD

The hash value is 230281421, which is congruent with 13 modulus 32
The prime associated 8191.

¡¡HEY!! With two different hashes $H$ we find the same prime! It can be strange, but the probability that it happen is greater that would be seem. Remember we work modulus 32, and the probability that a number is congruous with 13 modulus 32 (our case) is 3,125%.

--------------------------------------------------------------------------
Enter string: FEB3891C8E2C22FC055E542FDFE4B6590B6E6EEC396BE8180806898C1F19757D

The hash value is 1860602499, which is congruous with 3 modulus 32
The prime associated is 7.

--------------------------------------------------------------------------
Enter string: C1001DDFCE2AC0A5C5993A9ED184446E9EEEDE1E837F0B941A5F789F5F850086

The hash value is 779812730, which is congruent with 26 modulus 32
The prime associated is 67108879.

--------------------------------------------------------------------------
Enter string: 74E24FC8B807826026A524F1AF7A2EE28D3E024859C28F71C3A5D6244AFCF881

The hash value is 904128383, which is congruous with 31 modulus 32
The prime associated is 2147483647.


