extern crate web3;

use web3::transports::http::Http;
use web3::api::Personal;
use web3::futures::Future;
use web3::types::Address;

/// Returns the node registered addresses
pub fn list_accounts(http: &Http) -> Vec<Address> {
    let web3 = web3::Web3::new(http);
    let accounts = web3.personal().list_accounts().wait().unwrap();
    accounts
}

/// Creates an account given a password
pub fn create_account(password: &str, http: &Http) -> Address {
    let web3 = web3::Web3::new(http);
    let new_account = web3.personal().new_account(password).wait().unwrap();
    new_account
}

/// Unlocks an account given an account and password. Time unlocking not supported, pass None as Option<u16>
/// this will unlock the account just for the next transaction.
pub fn unlock_account(address: &Address, password: &str, duration: Option<u16>, http: &Http) -> bool {
    let web3 = web3::Web3::new(http);
    let result = web3.personal().unlock_account(*address, password, duration).wait().unwrap();
    result
}
